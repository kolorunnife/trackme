const dotenv = require('dotenv');
const axios = require('axios');
dotenv.config();
const { API_URL } = process.env;

test('Test device array', () => {
    expect.assertions(1);
    return axios.get(`${API_URL}/devices`)
        .then(resp => resp.data)
        .then(resp => {
            expect(resp[0].user).toEqual('sam');
        });
});

test('Test Account creation', () => {
    expect.assertions(1);
    return axios.post(`${API_URL}/register`, { username: 'kolade_olo', password: 'qwerty', isAdmin: false })
        .then(resp => resp.data)
        .then(resp => {
            expect(resp).toBe("User already exists.");
        });
});

test('Test Account Authenticate', () => {
    expect.assertions(1);
    return axios.post(`${API_URL}/authenticate`, { username: 'kolade_olo', password: 'qwerty'})
        .then(resp => resp.data)
        .then(resp => {
            expect(resp.message).toBe("Authenticated successfully");
        });
});

test('Test Device Creation', () => {
    expect.assertions(1);
    return axios.post(`${API_URL}/devices`, { name: 'Facebook Test Auth - ' + Math.round(Math.random() * 1000), user: 'Kolade Olorunnife', sensorData: []})
        .then(resp => resp.data)
        .then(resp => {
            expect(resp).toBe("Device added.");
        });
});