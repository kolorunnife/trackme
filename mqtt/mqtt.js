const mqtt = require('mqtt');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const randomCoordinates = require('random-coordinates');
const rand = require('random-int');

const app = express();
const { URL, USERSNAME, PASSWORD } = process.env;
const port = process.env.PORT || 5001;
const Device = require('./models/device');

mongoose.connect(process.env.MONGO_URL);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
    next();
});

const client = mqtt.connect(URL, {
    username: USERSNAME,
    password: PASSWORD
});

client.on('connect', () => {
    console.log('MQTT has successfully connected');
    client.subscribe("/sensorData");
});

app.get('/docs', (req, res) => {
    res.sendFile(`${__dirname}/public/generated-docs/index.html`);
});

/**
* @api {post} /send-command Publish message to '/command/<device id>'
* @apiGroup MQTT
* @apiSuccessExample {json} Success-Response:
* 
* Published new message!
*
* @apiErrorExample {json} Error-Response:
*
* undefined
*/
app.post('/send-command', (req, res) => {
    const { deviceId, command } = req.body;
    const topic = `/command/${deviceId}`;

    client.publish(topic, command, () => {
        res.send('Published new message!');
        console.log(`MQTT success -> Topic: ${topic}, Message: ${command}`);
    });
});

/**
* @api {put} /sensor-data Publish message to '/sensorData' with fake sensor data
* @apiGroup MQTT
* @apiSuccessExample {json} Success-Response:
* 
* published new message!
*
* @apiErrorExample {json} Error-Response:
*
* undefined
*/
app.put('/sensor-data', (req, res) => {
    const { deviceId } = req.body;
    const topic = `/sensorData`;

    const [lat, lon] = randomCoordinates().split(", ");
    const ts = new Date().getTime();
    const loc = { lat, lon };
    const temp = rand(20, 50);

    const message = JSON.stringify({ deviceId, ts, loc, temp });
    client.publish(topic, message, () => {
        res.send('published new message');
    });
});

//MQTT Listener
client.on('message', (topic, message) => {
    if (topic == "/sensorData") {
        console.log("Recieved Message: " + message);

        const data = JSON.parse(message);

        Device.findOne({ name: data.deviceId }, (err, document) => {
            if (err) {
                console.log(err);
            }

            const { sensorData } = document;
            const { ts, loc, temp } = data;

            //Pushing new sensor data
            sensorData.push({ ts, loc, temp });
            document.sensorData = sensorData;

            document.save(err => {
                if (err) console.log("ERROR: Failed to update sensor data", err);
                else console.log("Sensor data has been saved!");
            });
        });
    }
});

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});
