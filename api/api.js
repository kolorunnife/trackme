const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 5000;

//Models
const Device = require('./models/device');
const User = require('./models/user');

mongoose.connect(process.env.MONGO_URL);

app.listen(port, () => {
    console.log(`listening on port ${port}`);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
    next();
});

app.use(express.static(`${__dirname}/public`));
app.get('/docs', (req, res) => {
    res.sendFile(`${__dirname}/public/generated-docs/index.html`);
});


/**
* @api {get} /api/devices An array of all devices
* @apiGroup Device
* @apiSuccessExample {json} Success-Response:
* [
    * {
    * "_id": "dsohsdohsdofhsofhosfhsofh",
    * "name": "Mary's iPhone",
    * "user": "mary",
    * "sensorData": [
    * {
    * "ts": "1529542230",
    * "temp": 12,
    * "loc": {
    * "lat": -37.84674,
    * "lon": 145.115113
    * }
    * },
    * {
    * "ts": "1529572230",
    * "temp": 17,
    * "loc": {
    * "lat": -37.850026,
    * "lon": 145.117683
    * }
    * }
    * ]
    * }
    * ]
    * @apiErrorExample {json} Error-Response:
    * {
    *   "User does not exist"
    * }
*/
app.get('/api/devices', (req, res) => {
    Device.find({}, (err, devices) => {
        return err ? res.send(err) : res.send(devices);
    });
});

/**
* @api {get} /api/devices/:deviceId/device-history A device's sensor data
* @apiGroup Device
* @apiSuccessExample {json} Success-Response:
* [
    {"sensorData":[
        {"ts":"1529545935",
        "temp":14,
        "loc":{"lat":-37.839587,"lon":145.101386}
    ]}
  ]
*
* @apiErrorExample {json} Error-Response:
* []
*/
app.get('/api/devices/:deviceId/device-history', (req, res) => {
    const { deviceId } = req.params;
    Device.findOne({ "_id": deviceId }, (err, document) => {
        return err ? res.send(err) : res.send(document.sensorData);
    });
});

/**
* @api {get} /api/users/:user/devices An array of all of a user's devices
* @apiGroup Device
* @apiSuccessExample {json} Success-Response:
* [
    {"sensorData":[{"ts":"1529545935","temp":14,"loc":{"lat":-37.839587,"lon":145.101386}}],
    "_id":"5d29527836a05e4aabbe6366",
    "name":"Bob's iPhone",
    "user":"kol_olo",
    "id":"3"}
    ]
*
* @apiErrorExample {json} Error-Response:
* []
*/
app.get('/api/users/:user/devices', (req, res) => {
    const { user } = req.params;
    Device.find({ "user": user }, (err, devices) => {
        return err ? res.send(err) : res.send(devices);
    });
});

/**
* @api {post} /api/devices Creating a new device document
* @apiGroup Device
* @apiSuccessExample {json} Success-Response:
* {
    Device added.
}
*
* @apiErrorExample {json} Error-Response:
* []
*/
app.post('/api/devices', (req, res) => {
    const { name, user, sensorData } = req.body;

    const newDevice = new Device({
        name,
        user,
        sensorData
    });

    newDevice.save(err => {
        return err
            ? res.send(err)
            : res.send("Device added.");
    });
});

/**
* @api {post} /api/authenticate Authenticate user login details
* @apiGroup User
* @apiSuccessExample {json} Success-Response:
* 
{
    "success": "true",
    "message": "Authenticated successfully",
    "isAdmin": boolean
}
*
* @apiErrorExample {json} Error-Response:
* {
    User does not exist.
}
*/
app.post('/api/authenticate', (req, res) => {
    const { username, password } = req.body;

    User.findOne({ username: username }, (err, document) => {
        if (err) return err;

        if (document == undefined) {
            res.send("User does not exist.");
            return;
        }

        if (document.password != password) res.send("Invalid password");

        return res.json({
            success: true,
            message: 'Authenticated successfully',
            isAdmin: document.isAdmin
        });
    });
});

/**
* @api {post} /api/register Create new login details
* @apiGroup User
* @apiSuccessExample {json} Success-Response:
* 
{
    "success": "true",
    "message": "Created new user",
}
*
* @apiErrorExample {json} Error-Response:
* {
    User already exists.
}
*/
app.post('/api/register', (req, res) => {
    const { username, password, isAdmin } = req.body;

    User.findOne({ username: username }, (err, document) => {
        if (err) return err;

        if (document != undefined) {
            res.send("User already exists.");
            return;
        }

        const newUser = new User({
            username,
            password,
            isAdmin
        });

        newUser.save(err => {
            return err
                ? res.send(err)
                : res.json({
                    success: true,
                    message: 'Created new user'
                });
        });
    });
});

