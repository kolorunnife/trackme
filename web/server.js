const express = require('express');
const passport = require('passport'), FacebookStrategy = require('passport-facebook').Strategy;
const app = express();
const cors = require('cors');

const port = process.env.PORT || 5000;
const base = `${__dirname}/public`;

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
    next();
});
app.use(express.static('public'));

app.use(passport.initialize());
app.use(passport.session());

passport.use(new FacebookStrategy({
    clientID: 715215075605510,
    clientSecret: '3bd53c6624fa884dc5a84656450a8e61',
    callbackURL: "https://sit209-trackme.herokuapp.com/auth/facebook/callback"
},
    function (accessToken, refreshToken, profile, done) {
        return done(null, profile);
    }
));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});


app.get('/auth/facebook', passport.authenticate('facebook', { session: false }));

app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }),
    function (req, res) {
        console.log(req.user.displayName);
        res.send(`
            <script>
                localStorage.setItem('user', '${req.user.displayName}');
                window.location.href = 'https://sit209-trackme.herokuapp.com';
            </script>
        `);
});

/*
 * ROUTES
*/
app.get('/', (req, res) => {
    res.sendFile(`${base}/device-list.html`);
});

app.get('/register-device', (req, res) => {
    res.sendFile(`${base}/register-device.html`);
});

app.get('/send-command', (req, res) => {
    res.sendFile(`${base}/send-command.html`);
});

app.get('/about', (req, res) => {
    res.sendFile(`${base}/about-me.html`);
});

app.get('/register', (req, res) => {
    res.sendFile(`${base}/register.html`);
});

app.get('/login', (req, res) => {
    res.sendFile(`${base}/login.html`);
});

//Error 404
app.get('*', (req, res) => {
    res.sendFile(`${base}/404.html`);
});

//Web Listener\\
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});